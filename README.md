# Threespot Sass Patterns Repo

A useful collection of Sass mixins and methodologies. 

## New additions: 

- Should be added in a new folder 
- Folder naming should be category-detail(-detail) (example: mq-retina, font-sizing-rem)
- Include a `.scss` file showing the mixin or methodology demo (scss syntax *only*)
- Include a ReadMe file which at least briefly explains usage 
- Include proper attribution in ReadMe file if appropriate 
- If you have a better mixin to replace one currently in the repo, please get in touch with whomever uploaded the previous mixin to discuss a collaboration and/or replacement.

<hr />

*Seperate `.scss` file is helpful for those using [fetch](http://net.tutsplus.com/articles/news/introducing-nettuts-fetch/?search_index=4) or any other program which grabs content/snippets.